class Dosya():
  def __init__(self) -> None:
      with  open("./file-operations/metin.txt","r",encoding="utf-8") as f:
        content=f.read()
        words=content.split()
        self.just_words=list()
        for i in words:
          i = i.strip(".")
          i = i.strip(",")
          i = i.strip("\n")
          i = i.strip(" ")

          self.just_words.append(i)
        print(self.just_words)
  def all_words(self):
    words_set=set()
    for i in self.just_words:
      words_set.add(i.lower())
    print("All Words.....")
    for i in words_set:
      print(i)
      print("-------------------------")
  def words_seq(self):
    self.words_dict=dict()
    for i in self.just_words:
      if i.lower() in self.words_dict:
        self.words_dict[i.lower()] += 1
      else:
        self.words_dict[i.lower()] = 1
    for word,number in self.words_dict.items():
      print("The \"{}\" word is \'{}\' times pass.".format(word,number))
      print("*******************************************")
           
  def find_word(self):
    word= input("Bir kelime girin: ")
    times=0
    for i in self.just_words:
      if i.lower() == word.lower():
        times += 1
      elif i.lower().find(word)>-1:
        times += 1
    if times == 0:
      print("There is no words like this.")
    else:
      print("{} times pass".format(times))

dostoyevski=Dosya()

dostoyevski.all_words()
dostoyevski.words_seq()

dostoyevski.find_word()