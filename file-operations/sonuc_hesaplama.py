def sonuc_hesapla(liste):
  a=liste[:-1].split(",")
  isim=a[0]
  sonuc=int(a[1])*.3+int(a[2])*.3+int(a[3])*.4
  if sonuc>=60:
    with open("./file-operations/gecenler.txt","a+",encoding="utf-8") as gecenler:
      gecenler.write(isim + " Geçti. Notu: " + str(sonuc)+'\n')
    return isim + " Geçti. Notu: " + str(sonuc)
  else:
    with open("./file-operations/kalanlar.txt","a+",encoding="utf-8") as kalanlar:
      kalanlar.write(isim + " Kaldı. Notu: " + str(sonuc)+'\n')
    return isim + " Kaldı. Notu: " + str(sonuc)

with open ("./file-operations/notlar.txt","r",encoding="utf-8") as file:
  sonuclar=[]
  for i in file:
    sonuclar.append(sonuc_hesapla(i))

  with open("./file-operations/sonuclar.txt","w",encoding="utf-8") as file2:
    for i in sonuclar:
      file2.write(i+"\n")