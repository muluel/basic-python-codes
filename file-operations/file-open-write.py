f = open("./file-operations/bilgiler.txt","w",encoding="utf-8") # Dosyanin uzerine yazar.
f.write("f text\n")
f.write("muhammed uluel\n")
f.write("Serkan uluel\n")
f.close()


f2 = open("./file-operations/bilgiler.txt","a",encoding="utf-8") # Dosyaya ekler. (a) Append
f2.write("\nF2 teXT\n")
f2.write("Hello filesŞ")
f2.close()


# For ile okuma
print("_______File Read With For________")
fr2 = open("./file-operations/bilgiler.txt","r",encoding="utf-8")
for i in fr2:
  print(i,end="")
fr2.close()

# Read() ve readline() ile dosya okuma
print("_______File Read With READ()________")

fr=open("./file-operations/bilgiler.txt","r",encoding="utf-8")
print(fr.read(10))
print(fr.readline())
print(fr.read(10))
frlist=fr.readlines()
print("READLINES",frlist)
print(fr.read(50))
print("_____________")
print(fr.read(50))
print(fr.read(50))
fr.close()

# with ile dosya okuma, seek,tell
print("_______File Read With READ()________")
with  open("./file-operations/bilgiler.txt","r",encoding="utf-8") as file:
  print(file.tell())
  print(file.seek(5))
  a = file.readlines()
  for i in a:
    print(i,end="")
  print([i for i in a])


# with ile dosya okuma seek,write ,"r+" and lists
print("_______File Read With R+()________")
with  open("./file-operations/bilgiler.txt","r+",encoding="utf-8") as file:
  file.seek(7)
  file.write("Jr.Dev.\nm")
  file.seek(0)
  lines=file.readlines()
  print(lines)
  lines.insert(3,"Sales Manager\n")
  file.seek(0)
  file.writelines(lines)
  for line in lines:
    print(line,end="")
  