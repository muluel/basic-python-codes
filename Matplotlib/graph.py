import matplotlib.pyplot as plt
import numpy as np

x = np.arange(1,6)
y = np.arange(2,11,2)

fig = plt.figure()

axes = fig.add_axes([0.1,0.1,0.8,0.8])
axes2 = fig.add_axes([0.2,0.5,0.2,0.2])

axes.plot(y,x**4)
axes.set_xlabel("outer X")
axes.set_ylabel("outer y")
axes.set_title('Outer graph')

axes2.plot(x**5,y)
axes2.set_xlabel("inner X")
axes2.set_ylabel("inner y")
axes2.set_title('Inner graph')
plt.show()