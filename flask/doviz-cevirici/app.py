from flask import Flask, render_template,request
import requests

app = Flask(__name__)
api_key='3b26183123c8809e9cf900baf981342b'
url='http://data.fixer.io/api/latest?access_key='+api_key

@app.route("/",methods=["GET","POST"])
def index():
  if request.method == "POST":
    firstCurrency=request.form.get('firstCurrency')
    secondCurrency=request.form.get('secondCurrency')
    amount=request.form.get('amount')

    response=requests.get(url)
    app.logger.info(response)

    infos = response.json()
    firstValue=infos["rates"][firstCurrency]
    secondValue=infos["rates"][secondCurrency]
    result=(secondValue / firstValue) * float(amount)

    currencyInfo={"firstCurrency":firstCurrency,"secondCurrency":secondCurrency,"amount":amount,"result":result}
  
    app.logger.info(infos)
  else:
    return render_template("index.html")
  return render_template("index.html",info=currencyInfo)


if __name__ == "__main__":
  app.run(debug=True)