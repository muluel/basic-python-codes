from functools import  wraps
from flask import flash, session,redirect,url_for

def login_required(f):
  @wraps(f)
  def decorated_func(*args, **kwargs):
    if "logged_in" in session:
      return f(*args, **kwargs)
    else:
      flash(u"Bu sayfayı görebilmek için giriş yapmanız gerekiyor!", "danger")
      return redirect(url_for("login"))
  return decorated_func