from flask import Flask, render_template,flash,redirect,url_for,session,logging,request
from flask_mysqldb import MySQL
from passlib.hash import sha256_crypt
from forms import  RegisterForm,LoginForm,AddArticleForm
app = Flask(__name__)

app.config["MYSQL_HOST"] = "localhost"
app.config["MYSQL_USER"] = "root"
app.config["MYSQL_PASSWORD"] = ""
app.config["MYSQL_DB"] = "ybblog"
app.config["MYSQL_CURSORCLASS"] = "DictCursor"

app.config["SECRET_KEY"]="Thisisthesecrettedkey"

mysql = MySQL(app)

@app.route("/")
def index():
  return render_template("index.html")

@app.route("/about")
def about():
  return render_template("about.html")

@app.route("/articles")
def articles():
  cursor=mysql.connection.cursor()
  query="Select * from articles"
  cursor.execute(query)
  articles=cursor.fetchall()
  return render_template("articles.html",articles=articles)
  
@app.route("/article/<string:id>")
def article(id):
  cursor=mysql.connection.cursor()
  query= "select * from articles where id=%s"
  cursor.execute(query,(id,))

  article=cursor.fetchone()
  cursor.close()
  return render_template("article.html",article=article)

@app.route("/register",methods=["GET","POST"])
def register():
  form=RegisterForm(request.form)
  if request.method=="POST" and form.validate():
    name = form.name.data
    username = form.username.data
    email = form.email.data
    password = sha256_crypt.encrypt(form.password.data)

    cursor=mysql.connection.cursor()
    query="Insert into users(name,email,username,password) values (%s,%s,%s,%s)"
    try:
      cursor.execute(query,(name,email,username,password))
    except:
      flash(u"Böyle bir kullanıcı mevcut.","danger")
      flash(u"Lütfen e-posta ve kullanıcı adını kontrol edin.","info")
      return redirect(url_for("register"))
    mysql.connection.commit()
    cursor.close()
    flash(u"Başarı ile kayıt oldunuz.","success")
    return redirect(url_for("login"))
  else:
    # query = "select username from users"
    # cursor=mysql.connection.cursor()
    # cursor.execute(query)
    # users=cursor.fetchall()
    return render_template("register.html",form=form)


@app.route("/login",methods=["GET","POST"])
def login():
  form=LoginForm(request.form)
  if request.method=="POST":
    username=form.username.data
    password=form.password.data
    cursor=mysql.connection.cursor()
    query="Select * from users where username = %s "
    user = cursor.execute(query,(username,))
    if user>0:
      data=cursor.fetchone()
      real_password=data["password"]
      name=data["name"]
      if sha256_crypt.verify(password,real_password):
        flash("Başarı ile giriş yaptınız.","success")
        session["logged_in"]=True
        session["username"]=username
        session["name"] = name

        return redirect(url_for("index"))
      else:
        flash(u"Böyle bir kullanıcı yok","danger")
        return redirect("login")
    else:
      flash(u"Böyle bir kullanıcı yok","danger")
      return redirect("login")
  return render_template("login.html",form=form)

@app.route("/logout")
def logout():
  session.clear()
  flash(u"Oturumunuz kapatılmıştır.","success")
  return redirect(url_for("index"))

from decorators import login_required
@app.route("/dashboard")
@login_required
def dashboard():
  cursor=mysql.connection.cursor()
  author=session["username"]
  query="select * from articles where author = %s"
  result=cursor.execute(query,(author,))
  if result>0:
    articles=cursor.fetchall()
    return render_template("dashboard.html",articles=articles)
  else:
    return render_template("dashboard.html")

@app.route("/addarticle",methods=["GET","POST"])
def addarticle():
  form=AddArticleForm(request.form)
  if request.method=="POST" and form.validate():
    title=form.title.data
    content=form.content.data
    author=session["username"]

    cursor=mysql.connection.cursor()
    query="Insert into articles (title,author,content) values(%s,%s,%s)"
    cursor.execute(query,(title,author,content))
    mysql.connection.commit()
    cursor.close()

    flash(u"Makale başarı ile eklendi.","success")
    
    return redirect(url_for("dashboard"))
  return render_template("addarticle.html",form=form)

@app.route("/delete/<string:id>")
@login_required
def delete(id):
  cursor=mysql.connection.cursor()
  author=session["username"]
  query="Delete from articles where id = %s and author = %s"
  cursor.execute(query,(id,author))
  mysql.connection.commit()
  cursor.close()
  flash(u"Makale silindi")

  return redirect(url_for("dashboard"))

@app.route("/edit/<string:id>",methods=["POST","GET"])
@login_required
def update(id):
  if request.method=="GET":
    cursor=mysql.connection.cursor()
    author=session["username"]
    query="Select * from articles where id = %s and author=%s"
    result=cursor.execute(query,(id,author))
    if result==0:
      flash(u"Böyle bir makale yok veya bu işleme yetkiniz yok.","danger")
      cursor.close()
      return redirect(url_for("index"))
    else:
      article=cursor.fetchone()
      form=AddArticleForm()
      form.title.data=article["title"]
      form.content.data=article["content"]
      cursor.close()
      return render_template("update.html",form=form)

  else:
    form = AddArticleForm(request.form)
    newtitle = form.title.data
    newContent = form.content.data
    author = session["username"]
    query2 = "update articles set title = %s , content = %s where id = %s "
    cursor=mysql.connection.cursor()
    cursor.execute(query2,(newtitle,newContent,id))
    mysql.connection.commit()
    cursor.close()
    flash(u"Makale güncellendi","success")

    return redirect(url_for("dashboard"))

@app.route("/search",methods=["POST","GET"])
def search():
  if request.method == "GET":
    return redirect(url_for("index"))
  else:
    keyword = request.form.get("keyword")
    cursor = mysql.connection.cursor()
    query = "Select * from articles where title like '%"+ keyword + " %'"
    result = cursor.execute(query)
    if result==0:
      flash(u"Aranan kelimeye uygun bir makale bulunamadı.","info")
      return(redirect(url_for("articles")))
    else:
      articles=cursor.fetchall()
      return render_template("articles.html",articles=articles)
if __name__ == '__main__':
  app.run(debug=True)
