from wtforms import StringField,TextAreaField,PasswordField,validators,Form
class RegisterForm(Form):
  name=StringField("Tam Ad",validators=[
    validators.Length(min=4,max=35,message="Adınız 4 ile 35 karakter arasında olmalıdır."),
    ])
  username=StringField("Kullanıcı Adı",validators=[validators.Length(min=5,max=35),validators.DataRequired(True)])
  email=StringField("E-posta",validators=[validators.Email(message="Lütfen geçerli bir e-posta giriniz!")])
  password=PasswordField("Parola",validators=[
    validators.DataRequired(message="Bu alan zorunludur."),
    validators.EqualTo(fieldname="confirm",message="Girilen değerler eşleşmiyor")
  ])
  confirm=PasswordField("Parola Doğrula")

class LoginForm(Form):
  username=StringField("Kullanıcı Adı")
  password=PasswordField("Parola")

class AddArticleForm(Form):
  title=StringField("Başlık",validators=[
    validators.length(min=10,max=100),
  ])
  content=TextAreaField("İçerik",validators=[
    validators.length(min=20)
  ])