import sqlite3

class Book():
  def __init__(self,name,writer,publisher,genre,edition,number_of_pages) -> None:
    self.name = name
    self.writer = writer
    self.publisher = publisher
    self.genre = genre
    self.edition = edition
    self.number_of_pages = number_of_pages

  def __str__(self) -> str:
    return "Book Name: {}\n- Writer: {}\n- Publisher: {}\n- Number of Pages: {}\n- Genre: {}\n- Edition: {}\n".format(self.name,self.writer,self.publisher,self.number_of_pages,self.genre,self.edition)

class Library():
  def __init__(self) -> None:
    self.connection()

  def connection(self) -> None:
    self.con = sqlite3.connect("./library/library.db")
    self.cursor = self.con.cursor()
    q = "Create Table If not exists books (name TEXT,writer TEXT,publisher TEXT,number_of_pages INT,genre TEXT,edition INT)"
    self.cursor.execute(q)
    self.con.commit()

  def close_connection(self) -> None:
    self.con.close()

  def show_books(self) -> None:
    q = "Select * from books"
    self.cursor.execute(q)
    books = self.cursor.fetchall()

    if (len(books)==0):
      print("There is no book in library")
    else:
      for n in books:
        book = Book(n[0],n[1],n[2],n[3],n[4],n[5])
        print(book)

  def search_book(self,name) -> None:
    q="Select * from books where name like '%{}%'"
    self.cursor.execute(q.format(name))
    books=self.cursor.fetchall()

    if (len(books)==0):
      print("There is no books like {} in this library.".format(name))
    else:
      for i in books:
        book=Book(i[0],i[1],i[2],i[3],i[4],i[5])
        print(book)

  def add_book(self,book) -> None:
    q = "Insert into books Values(?,?,?,?,?,?)"
    self.cursor.execute(q,book)
    self.con.commit()

  def delete_book(self,name) -> None:
    sq="Select * from books where name=?"
    self.cursor.execute(sq,(name,))
    books=self.cursor.fetchall()
    if len(books)==0:
      return print("""
      Kitabın adını doğru yazdığınızdan emin olun!
      Büyük küçük harflere dikkat ediniz!
      """)
    else:
      q = "Delete from books where name = '{}'"
      self.cursor.execute(q.format(name))
      self.con.commit()
      return print("The book deleted.")


  def increase_edition(self,name) -> None:
    q="Select edition from books where name like '%{}%'"
    self.cursor.execute(q.format(name))
    books=self.cursor.fetchall()
    if (len(books)==0):
      print("There is no books like {} in this library.".format(name))
    else:
      edition=books[0][0]
      edition+=1
      q2="Update books set edition = {} where name like '%{}%'"
      self.cursor.execute(q2.format(edition,name))
      print("Edition increased.")
      self.cursor.execute(q.format(name))
      books=self.cursor.fetchall()
      print("New Edition is : ",books[0][0])
      self.con.commit()

  def update_edition(self,name,edition) -> None:
    q="Select edition from books where name like '%{}%'"
    self.cursor.execute(q.format(name))
    books=self.cursor.fetchall()
    if (len(books)==0):
      print("There is no books like {} in this library.".format(name))
    else:
      edition=edition
      q2="Update books set edition = {} where name like '%{}%'"
      self.cursor.execute(q2.format(edition,name))
      print("Edition updated.")
      self.cursor.execute(q.format(name))
      books=self.cursor.fetchall()
      print("New Edition is : ",books[0][0])
      self.con.commit()

  def update_number_of_pages(self,name,number_of_pages) -> None:
    q="Select * from books where name like '%{}%'"
    self.cursor.execute(q.format(name))
    books=self.cursor.fetchall()
    if (len(books)==0):
      print("There is no books like {} in this library.".format(name))
    else:
      number_of_pages=number_of_pages
      q2="Update books set number_of_pages = {} where name like '%{}%'"
      self.cursor.execute(q2.format(number_of_pages,name))
      print("Number of pages of {} was updated.".format(name))
      self.cursor.execute(q.format(name))
      books=self.cursor.fetchall()
      print("New number of pages of {} is : {}".format(name,books[0][5]))
      self.con.commit()

  def update_genre(self,name,new_genre) -> None:
    q="Select * from books where name like '%{}%'"
    self.cursor.execute(q.format(name))
    books=self.cursor.fetchall()
    if (len(books)==0):
      print("There is no books like {} in this library.".format(name))
    else:
      new_genre=new_genre
      q2="Update books set genre = ? where name like '%{}%'".format(name)
      self.cursor.execute(q2,(new_genre,))
      print("The genre of {} was updated.".format(name))
      self.cursor.execute(q.format(name))
      books=self.cursor.fetchall()
      print("The genre of {} is : {}".format(name,books[0][3]))
      self.con.commit()

  def update_name(self,name,new_name) -> None:
    q="Select * from books where name like '%{}%'"
    self.cursor.execute(q.format(name))
    books=self.cursor.fetchall()
    if (len(books)==0):
      print("There is no books like {} in this library.".format(name))
    else:
      db_name=new_name
      q2="Update books set name = ? where name like '%{}%'".format(name)
      self.cursor.execute(q2,(db_name,))
      print("The name of {} was updated.".format(name))
      self.cursor.execute(q.format(name))
      books=self.cursor.fetchall()
      print("The name of {} now : {}".format(name,books[0][0]))
      self.con.commit()

  def update_publisher(self,name,new_publisher) -> None:
    q="Select * from books where name like '%{}%'"
    self.cursor.execute(q.format(name))
    books=self.cursor.fetchall()
    if (len(books)==0):
      print("There is no books like {} in this library.".format(name))
    else:
      new_publisher=new_publisher
      q2="Update books set publisher = ? where name like '%{}%'".format(name)
      self.cursor.execute(q2,(new_publisher,))
      print("The publisher of {} was updated.".format(name))
      self.cursor.execute(q.format(name))
      books=self.cursor.fetchall()
      print("The publisher of {} is : {}".format(name,books[0][2]))
      self.con.commit()
  