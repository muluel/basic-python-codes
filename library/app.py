from library import Library
import time

print("""
**************************
Welcome To Library

1. Show Books

2. Search books

3. Add Book

4. Delete Book

5. Increase Edition

6. Update Edition

7. Update Number of Pages

8. Update Genre

9. Update Name

10. Update Publisher

For quit press "q".
***************************
""")
library=Library()
while True:
  action=input("What would you like to do : ")
  if (action=="q"):
    print("The app is closing...")
    break
  elif(action=="1"):
    library.show_books()
  elif(action=="2"):
    name = input("Which book do you want to search : ")
    print("Books are searching...")
    time.sleep(2)
    library.search_book(name)
  elif(action=="3"):
    name=input("Name : ")
    writer=input("Writer : ")
    publisher=input("Publisher : ")
    number_of_pages=int(input("Number_of_pages : "))
    genre=input("Genre : ")
    edition=int(input("Edition : "))
    new_book = (name,writer,publisher,genre,edition,number_of_pages)
    print("Adding book..")
    time.sleep(2)
    library.add_book(new_book)
    print("Book was added.")
  elif(action=="4"):
    name=input("Name : ")
    sure=input("Are you sure?(y/N)")
    if sure.lower()=="y":
      library.delete_book(name)
    else:
      print("Action is canceling.")
      time.sleep(2)
      print("Action cancelled.")
  elif(action=="5"):
    name = input("Name : ")
    library.increase_edition(name)
  elif(action=="6"):
    name = input("Name : ")
    edition = input("Edition : ")
    library.update_edition(name,edition)
  elif(action=="7"):
    name = input("Name : ")
    number_of_pages = input("Number of Pages : ")
    library.update_number_of_pages(name,number_of_pages)
  elif(action=="8"):
    name = input("Name : ")
    new_genre = input("New Genre : ")
    library.update_genre(name,new_genre)
  elif(action=="9"):
    name = input("Name : ")
    new_name = input("New Name : ")
    library.update_name(name,new_name)
  elif(action=="10"):
    name = input("Name : ")
    new_publisher = input("New Publisher : ")
    library.update_publisher(name,new_publisher)
  else:
    print("Error 404! Action not found.")
