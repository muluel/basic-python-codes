from tkinter import *

# girilen sayiyi inputa yazar
def yaz(x):
    s = len(giris.get())
    giris.insert(s,str(x))
    #print(x)

#toplama cikarma gibi islemlerin sayisal degerini tutar
hesap = 5
#ilk girilen sayi degeridir islemden sonra burasi ilk sayi olmaya devam eder.
s1 = 0
# son girilen islemi ve son girilen sayiyi global degiskende store eder.
def islemler(x):
    global hesap
    hesap = x
    global s1
    s1 = float(giris.get())
    print(hesap)
    print(s1)
    giris.delete(0,'end')

s2 = 0
# esittire basildigi zaman son girilen sayiyi alir ve s2 degiskenine atar.
# global hesap degiskeninden islemi alir.
# global s1 sayisini alir.
# hesap hangi islemi temsil ediyor ise ona gore s1 ve s2 sayisini islme sokar.
# Sonucu entry(giris) bolumune insert eder.
def hesapla():
    global s2
    s2 = float(giris.get())
    print(s2)
    global hesap
    sonuc=0
    if(hesap==0):
        sonuc = s1 + s2
    elif(hesap==1):
        sonuc = s1 - s2
    elif (hesap == 2):
        sonuc = s1 * s2
    elif (hesap == 3):
        sonuc = s1 / s2
    giris.delete(0,'end')
    giris.insert(0,str(sonuc))


pencere = Tk() # Pencereyi acar.
pencere.geometry("230x300") # pencerenin boyutunu ayarlar

# input alanini olusturur
giris = Entry(font="Verdana 14 bold",width=13,justify=RIGHT)
# input alaninin yerini belirler
giris.place(x=20,y=20)

b = []
# Butonlari olusturur ve yaz fonksiyonu ile inputa text degerini yazmasini saglar
for i in range(1,10):
    b.append(Button(text=str(i),font="Verdana 14 bold",command=lambda x=i:yaz(x)))

sayac=0

#Butonlarin yerini ayarlar.
for i in range(0,3):
    for j in range(0,3):
        b[sayac].place(x=20+j*50,y=50+i*50)
        sayac += 1

islem = []

# islem butonlarini ve islemler fonksiyonu ile global olarak girilen islemi ve sayiyi tutmasini saglar.
for i in range(0,4):
    islem.append(Button(font="Verdana 14 bold",width=2,command=lambda x=i:islemler(x)))

# islemlerin text degerini burada ekleriz.
islem[0]['text'] = "+"
islem[1]['text'] = "-"
islem[2]['text'] = "*"
islem[3]['text'] = "/"

# islemleri konumunu ayarlar.
for i in range(0,4):
    islem[i].place(x=170,y=50+50*i)

# 0 butonunu ayarlar ve konumuna yerlestirir.
sb = Button(text="0",font="Verdana 14 bold",command=lambda x=0:yaz(x))
sb.place(x=20,y=200)

# '.' butonunu ayarlar ve konumuna yerlestirir.
nb = Button(text=".",font="Verdana 14 bold",width=2,command=lambda x=".":yaz(x))
nb.place(x=70,y=200)

# "=" butonunu ayarlar ve konumuna yerlestirir. Butona basildiginda hesapla komutu calisarak globalde tutulan islemi ve sayisi alip son girilen sayi ile isleme sokar.
eb = Button(text="=",fg ="RED",font="Verdana 14 bold",command=hesapla)
eb.place(x=120,y=200)

# pencereyi acik tutar.
pencere.mainloop()