import sys 

from PyQt5 import QtWidgets, QtGui

def Pencere():
  app = QtWidgets.QApplication(sys.argv)

  pencere = QtWidgets.QWidget() #
  pencere.setWindowTitle('PyQt5 - Lesson 1')
  pencere.setGeometry(100,100,500,500)

  tag=QtWidgets.QLabel(pencere) #
  tag.setText("Hello World")
  tag.move(215,30)

  # tag2=QtWidgets.QLabel(pencere) #
  # tag2.setPixmap(QtGui.QPixmap("python.png"))

  buttton=QtWidgets.QPushButton(pencere) #
  buttton.setText("Submit")
  buttton.move(205,50)

  okay=QtWidgets.QPushButton("Okay")
  cancel=QtWidgets.QPushButton("Cancel")

  h_box=QtWidgets.QHBoxLayout()

  h_box.addStretch()
  h_box.addWidget(okay)
  h_box.addWidget(cancel)

  v_box=QtWidgets.QVBoxLayout()

  v_box.addWidget(tag)
  v_box.addWidget(buttton)
  v_box.addStretch()
  v_box.addLayout(h_box)
  # v_box.addWidget(okay)
  # v_box.addWidget(cancel)

  # pencere.setLayout(h_box)
  pencere.setLayout(v_box)
  pencere.show()
  sys.exit(app.exec_())

Pencere()