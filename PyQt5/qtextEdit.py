import sys
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QWidget,QApplication,QRadioButton,QLabel,QPushButton,QVBoxLayout,QTextEdit

class Pencere(QWidget):
  def __init__(self):
    super().__init__()
    self.init_ui()

  def init_ui(self):
    self.text_area=QTextEdit()

    self.clear = QPushButton("Clear")

    vbox = QVBoxLayout()
    vbox.addWidget(self.text_area)
    vbox.addWidget(self.clear)

    self.setLayout(vbox)

    self.setWindowTitle("QtextEdit Box")
    self.clear.clicked.connect(self.click)
    self.show()
  
  def click(self):
    self.text_area.clear()

app = QApplication(sys.argv)

pencere= Pencere()

sys.exit(app.exec_())