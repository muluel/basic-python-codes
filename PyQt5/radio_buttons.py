import sys
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QWidget,QApplication,QRadioButton,QLabel,QPushButton,QVBoxLayout

class Pencere(QWidget):
  def __init__(self):
    super().__init__()
    self.init_ui()

  def init_ui(self):
    self.label = QLabel("Which lang is your favourite?")
    self.python = QRadioButton("Python")
    self.java = QRadioButton("Java")
    self.c = QRadioButton("C")

    self.text_area=QLabel("")

    self.button = QPushButton("Send")

    vbox = QVBoxLayout()
    vbox.addWidget(self.label)
    vbox.addWidget(self.python)
    vbox.addWidget(self.c)
    vbox.addWidget(self.java)
    vbox.addStretch()
    vbox.addWidget(self.text_area)
    vbox.addWidget(self.button)

    self.setLayout(vbox)

    self.setWindowTitle("Radio Box")
    self.button.clicked.connect(lambda : self.click(self.python.isChecked(),self.c.isChecked(),self.java.isChecked(),self.text_area))
    self.show()
  
  def click(self,python,c,java,text_area):
    if python:
      text_area.setText("Python")
    elif c:
      text_area.setText("C")
    elif java:
      text_area.setText("Java")
    else:
      text_area.setText("You have to select one of above!")

app = QApplication(sys.argv)

pencere= Pencere()

sys.exit(app.exec_())