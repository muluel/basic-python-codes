import sys
from PyQt5 import QtWidgets

class Pencere(QtWidgets.QWidget):
  
  def __init__(self):
    super().__init__()
    self.init_ui()

  def init_ui(self):
    self.input = QtWidgets.QLineEdit()
    self.clear = QtWidgets.QPushButton("Clear")
    self.print = QtWidgets.QPushButton("Print")

    vbox = QtWidgets.QVBoxLayout()
    vbox.addWidget(self.input)

    innerHbox=QtWidgets.QHBoxLayout()
    innerHbox.addWidget(self.print)
    innerHbox.addWidget(self.clear)

    vbox.addLayout(innerHbox)
    vbox.addStretch()
    
    hbox = QtWidgets.QHBoxLayout()
    hbox.addStretch()
    hbox.addLayout(vbox)
    hbox.addStretch()

    self.setLayout(hbox)
    self.clear.clicked.connect(self.click)
    self.print.clicked.connect(self.click)
    self.show()

  def click(self):
    sender=self.sender()
    if sender == self.clear:
      self.input.clear()
    elif sender == self.print:
      print(self.input.text())

app = QtWidgets.QApplication(sys.argv)

window = Pencere()

sys.exit(app.exec_())