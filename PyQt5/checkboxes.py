import sys

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QWidget,QApplication,QCheckBox,QLabel,QPushButton,QVBoxLayout

class Pencere(QWidget):
  def __init__(self):
    super().__init__()
    self.init_ui()

  def init_ui(self):
    self.checkbox = QCheckBox("Do you Love Python?")
    self.text_area = QLabel("")
    self.button = QPushButton("Click Me")

    vbox = QVBoxLayout()
    vbox.addWidget(self.checkbox)
    vbox.addWidget(self.text_area)
    vbox.addWidget(self.button)

    self.setLayout(vbox)

    self.setWindowTitle("Check Box")
    self.button.clicked.connect(lambda : self.click(self.checkbox.isChecked(),self.text_area))
    self.show()
  
  def click(self,is_checked,text_area):
    if is_checked:
      text_area.setText("Nice!!")
    else:
      text_area.setText("Why? :(")

app = QApplication(sys.argv)

pencere= Pencere()

sys.exit(app.exec_())