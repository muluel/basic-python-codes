import sys
import os
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QWidget,QApplication,QRadioButton,QLabel,QPushButton,QVBoxLayout,QTextEdit,QFileDialog,QHBoxLayout
from matplotlib.pyplot import text

class Pencere(QWidget):
  def __init__(self):
    super().__init__()
    self.init_ui()

  def init_ui(self):
    self.text_area=QTextEdit()

    self.clear = QPushButton("Clear")
    self.open = QPushButton("Open")
    self.save = QPushButton("Save")

    hbox = QHBoxLayout()
    hbox.addWidget(self.clear)
    hbox.addWidget(self.open)
    hbox.addWidget(self.save)
    vbox = QVBoxLayout()
    vbox.addWidget(self.text_area)
    vbox.addLayout(hbox)

    self.setLayout(vbox)

    self.setWindowTitle("Notepad")
    self.clear.clicked.connect(self.clear_text)
    self.open.clicked.connect(self.open_file)
    self.save.clicked.connect(self.save_file)
    self.show()
  
  def clear_text(self):
    self.text_area.clear()
  def open_file(self):
    file = QFileDialog.getOpenFileName(self,"Open File",os.getenv("HOME"))
    with open(file[0],'r') as f:
      self.text_area.setText(f.read())
  def save_file(self):
    file=QFileDialog.getSaveFileName(self,"Save File",os.getenv("HOME"))
    with open(file[0],'w') as f:
      f.write(self.text_area.toPlainText())

app = QApplication(sys.argv)

pencere= Pencere()

sys.exit(app.exec_())