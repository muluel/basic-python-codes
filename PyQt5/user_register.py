import sys
import sqlite3
from PyQt5 import QtWidgets

class Pencere(QtWidgets.QWidget):
  
  def __init__(self):
    super().__init__()
    self.connection()
    self.init_ui()

  def connection(self):
    self.con=sqlite3.connect('db.db')
    self.cursor = self.con.cursor()
    self.cursor.execute('create table if not exists users (username TEXT,password TEXT)')
    self.con.commit()
    self.stackedLayout = QtWidgets.QStackedLayout()

  def init_ui(self):
    self.username = QtWidgets.QLineEdit()
    self.username.setPlaceholderText('username')
    self.password = QtWidgets.QLineEdit()
    self.password.setPlaceholderText('password')
    self.password.setEchoMode(QtWidgets.QLineEdit.Password)

    self.login = QtWidgets.QPushButton("Login")
    self.register_page = QtWidgets.QPushButton("Register")
    self.label = QtWidgets.QLabel("")

    vbox = QtWidgets.QVBoxLayout()
    vbox.addWidget(self.username)
    vbox.addWidget(self.password)
    vbox.addWidget(self.label)

    innerHbox=QtWidgets.QHBoxLayout()
    innerHbox.addWidget(self.login)
    innerHbox.addWidget(self.register_page)
    vbox.addLayout(innerHbox)

    vbox.addStretch()
    self.page1 = QtWidgets.QWidget()
    hbox = QtWidgets.QHBoxLayout()
    hbox.addStretch()
    hbox.addLayout(vbox)
    hbox.addStretch()
    self.page1.setLayout(hbox)
    self.stackedLayout.addWidget(self.page1)

    self.setWindowTitle("User Interface")
    self.login.clicked.connect(self.click)
    self.register_page.clicked.connect(self.click)

  def register_ui(self):
    self.username = QtWidgets.QLineEdit()
    self.username.setPlaceholderText('username')
    self.password = QtWidgets.QLineEdit()
    self.password.setPlaceholderText('password')
    self.password.setEchoMode(QtWidgets.QLineEdit.Password)
    self.password2 = QtWidgets.QLineEdit()
    self.password2.setPlaceholderText('Confirm password')
    self.password2.setEchoMode(QtWidgets.QLineEdit.Password)

    self.register = QtWidgets.QPushButton("register")
    self.login_page = QtWidgets.QPushButton("Login")
    self.label = QtWidgets.QLabel("")

    vbox = QtWidgets.QVBoxLayout()
    vbox.addWidget(self.username)
    vbox.addWidget(self.password)
    vbox.addWidget(self.password2)
    vbox.addWidget(self.label)

    innerHbox=QtWidgets.QHBoxLayout()
    innerHbox.addWidget(self.register)
    innerHbox.addWidget(self.login_page)
    vbox.addLayout(innerHbox)

    vbox.addStretch()
    self.page2 = QtWidgets.QWidget() 
    rhbox = QtWidgets.QHBoxLayout()
    rhbox.addStretch()
    rhbox.addLayout(vbox)
    rhbox.addStretch()
    self.page2.setLayout(rhbox)

    self.stackedLayout.addWidget(self.page2)
    self.setWindowTitle("User Interface")
    self.register.clicked.connect(self.click)
    self.login_page.clicked.connect(self.click)

  def click(self):
    sender = self.sender()
    if sender == self.login:
      username=self.username.text()
      password=self.password.text()

      self.cursor.execute("select * from users where username = ? and password = ?",(username,password))
      data = self.cursor.fetchall()

      if len(data)==0:
        self.label.setText("User doesn't exist.")
      else:
        self.label.setText(f"Welcome {username}" )
    elif sender == self.register_page:
      self.register_ui()
      self.stackedLayout.setCurrentIndex(1)
      print(self.stackedLayout.currentIndex())
    elif sender == self.login_page:
      self.init_ui()
      self.stackedLayout.setCurrentIndex(0)
      print(self.stackedLayout.currentIndex())
    elif sender == self.register:
      username=self.username.text()
      password=self.password.text()
      password2=self.password2.text()
      if password != password2:
        self.label.setText("Passwords not match.")
      else:
        self.cursor.execute('Insert into users (username,password) values(?,?)',(username,password))
        self.con.commit()
        self.init_ui()
        self.label.setText("Başarı ıle kayıt olundu")
    

    

app = QtWidgets.QApplication(sys.argv)
window = Pencere()
sys.exit(app.exec_())