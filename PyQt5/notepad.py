import sys
import os
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QWidget,QApplication,QPushButton,QVBoxLayout,QTextEdit,QFileDialog,QHBoxLayout,QAction,qApp,QMainWindow

class Menu(QMainWindow):
  def __init__(self, *args, **kwargs):
    super().__init__()
    menubar=self.menuBar()
    file= menubar.addMenu("File")
    edit= menubar.addMenu("Edit")
    open_file = QAction("Open File",self)
    open_file.setShortcut("Ctrl+O")
    save_file = QAction("Save File",self)
    save_file.setShortcut("Ctrl+S")
    quit = QAction("Quit",self)
    quit.setShortcut("Ctrl+Q")

    file.addAction(open_file)
    file.addAction(save_file)
    file.addAction(quit)

    find = edit.addMenu("Find and Replace")
    clear = QAction("Clear",self)
    edit.addAction(clear)
    search=QAction("Search",self)
    replace=QAction("Replace",self)

    find.addAction(search)
    find.addAction(replace)

    quit.triggered.connect(self.quit_program)

    file.triggered.connect(self.response)

    self.setWindowTitle("Notepad")
    self.show()

  def quit_program(self):
    qApp.quit()
  
  def response(self,action):
    if action.text=="Open File":
      pass
    elif action.text=="Save File":
      pass
    elif action.text=="Quit":
      pass

class Pencere(QWidget):
  def __init__(self):
    super().__init__()
    self.init_ui()

  def init_ui(self):
    self.text_area=QTextEdit()

    self.clear = QPushButton("Clear")
    self.open = QPushButton("Open")
    self.save = QPushButton("Save")

    hbox = QHBoxLayout()
    hbox.addWidget(self.clear)
    hbox.addWidget(self.open)
    hbox.addWidget(self.save)
    vbox = QVBoxLayout()
    vbox.addWidget(self.text_area)
    vbox.addLayout(hbox)

    self.setLayout(vbox)

    self.setWindowTitle("Notepad")
    self.clear.clicked.connect(self.clear_text)
    self.open.clicked.connect(self.open_file)
    self.save.clicked.connect(self.save_file)
    self.show()
  
  def clear_text(self):
    self.text_area.clear()
  def open_file(self):
    file = QFileDialog.getOpenFileName(self,"Open File",os.getenv("HOME"))
    with open(file[0],'r') as f:
      self.text_area.setText(f.read())
  def save_file(self):
    file=QFileDialog.getSaveFileName(self,"Save File",os.getenv("HOME"))
    with open(file[0],'w') as f:
      f.write(self.text_area.toPlainText())

app = QApplication(sys.argv)

pencere= Pencere()

menu = Menu()

sys.exit(app.exec_())