import sys
from PyQt5 import QtWidgets

class Pencere(QtWidgets.QWidget):
  
  def __init__(self):
    super().__init__()
    self.init_ui()

  def init_ui(self):
    self.text_area = QtWidgets.QLabel("Not clicked yet.")
    self.button = QtWidgets.QPushButton("Click")
    self.count = 0

    vbox = QtWidgets.QVBoxLayout()
    vbox.addWidget(self.button)
    vbox.addWidget(self.text_area)
    vbox.addStretch()
    
    hbox = QtWidgets.QHBoxLayout()
    hbox.addStretch()
    hbox.addLayout(vbox)
    hbox.addStretch()

    self.setLayout(hbox)
    self.button.clicked.connect(self.click)
    self.show()

  def click(self):
    self.count+=1
    self.text_area.setText(f'{str(self.count)} times clicked')

app = QtWidgets.QApplication(sys.argv)

window = Pencere()

sys.exit(app.exec_())