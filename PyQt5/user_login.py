import sys
import sqlite3
from PyQt5 import QtWidgets

class Pencere(QtWidgets.QWidget):
  
  def __init__(self):
    super().__init__()
    self.connection()
    self.init_ui()

  def connection(self):
    con=sqlite3.connect('db.db')
    self.cursor = con.cursor()
    self.cursor.execute('create table if not exists users (username TEXT,password TEXT)')
    con.commit()
    
  def init_ui(self):
    self.username = QtWidgets.QLineEdit()
    self.username.setPlaceholderText('username')
    self.password = QtWidgets.QLineEdit()
    self.password.setPlaceholderText('password')
    self.password.setEchoMode(QtWidgets.QLineEdit.Password)

    self.login = QtWidgets.QPushButton("Login")
    self.label = QtWidgets.QLabel("")

    vbox = QtWidgets.QVBoxLayout()
    vbox.addWidget(self.username)
    vbox.addWidget(self.password)
    vbox.addWidget(self.label)

    innerHbox=QtWidgets.QHBoxLayout()
    innerHbox.addWidget(self.login)
    vbox.addLayout(innerHbox)

    vbox.addStretch()
    
    hbox = QtWidgets.QHBoxLayout()
    hbox.addStretch()
    hbox.addLayout(vbox)
    hbox.addStretch()

    self.setLayout(hbox)
    self.setWindowTitle("User Interface")
    self.login.clicked.connect(self.click)
    self.show()

  def click(self):
    sender = self.sender()
    if sender == self.login:
      username=self.username.text()
      password=self.password.text()

      self.cursor.execute("select * from users where username = ? and password = ?",(username,password))
      data = self.cursor.fetchall()

      if len(data)==0:
        self.label.setText("User doesn't exist.")
      else:
        self.label.setText(f"Welcome {username}" )


app = QtWidgets.QApplication(sys.argv)
window = Pencere()
sys.exit(app.exec_())