a="deneme".upper()
b="deneme".lower()
c="deneme".replace("eme",'ama')
[print(x,y,end="\n") for x,y in list(enumerate((a,b,c)))]

d="Python".startswith("P")
e="Python".endswith("N")
print(d,e,sep="\n")

f="Python Programlama Dili".split("a")
print(f)
print("a".join(f))

g="                   Python             ".strip()
h="                   Python             ".lstrip()
j="                   Python             ".rstrip()
print(g,h,j,sep="#\n",end="#\n")

k=">>>>>>>>>>>Python>>>>>>>>>>>".strip('>')
print(k)

l="Count function".count("u")
m="Count function".count("u",3)
print(l,m,sep="\n")

n="find the find word".find("find")
o="find the find word".rfind("find")
p="find the find word".find("find",2)
print(n,o,p)