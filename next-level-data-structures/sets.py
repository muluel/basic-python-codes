# Sets return unorder data(when it creating another data type/s). 
# Data order will be random on every run.
x = {1,2,3,3}
print(type(x),x)
y = {1:2,2:3,3:4,3:4}
print(type(y),y,y[3])

x = set("Python Programlama Dili")
print(type(x), x)
for i in x:
  print(i)

x.add("Değildir")
print(x)
x.discard("Değildir")
print(x)

y=set(y)
print(type(y),y)
print(y.difference(x))

z=set("Python")
x.difference(z)
print(x)
z.difference_update(x)
print(z)
x.difference_update(z)
print(x)
print("################")
set1={1,2,4,5,6,78,4}
set2={1,2,3,4,5,6,7,8,9,10}
sets_intersection=set1.intersection(set2)
sets_difference=set2.difference(set1)
print(set1,set2,sets_intersection,sets_difference,sep="\n")
set2.intersection_update(set1)
print(set1,set2)
set1.difference_update(set2)
print(set1,set2)

set1={1,2,4,5,6,78,4}
set2={1,2,3,4,5,6,7,8,9,10}
set3={13,14,15}
set4={13,14}
print(set1.isdisjoint(set2))
print(set2.isdisjoint(set3))
print(set1.issubset(set2))
print(set4.issubset(set3))
print(set1,set2,set3,set4)
set2.update(set1) # update and union same but update return value also update the given set
print(set2,set1)
set5=set3.union(set4) # union return a value but not update the set
print(set3,set4,set5)