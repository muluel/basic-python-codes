print(bin(129)) # displays number in base 2(binary) Output 0b10000001
print(hex(128)) # displays numbers in base 16(hexadecimal) Output 0x80
print(abs(-1.8)) # displays numbers absolute value. Output 1.8
print(round(4.624,2)) # it Round a number to a given precition in decimal digits. Output 4.62 
print(max(3,5,6,3,7,3,7,89,8,3)) # output 89
print(min(3,5,6,3,7,3,7,89,8,3)) # Output 3
print(sum(3,5,7,1,4,123,32)) # It's sum the given numbers
print(pow(2,4)) # Output 16 (2**4)
