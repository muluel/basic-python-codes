liste = []

liste.append("A")

liste.extend(["B","C","D"])

liste.append(["B","C","D"])

liste.insert(0,0)

liste.insert(0,"Array Examples")

print(liste.pop())

print(liste.pop(1))

liste.remove("C")

print("index of \"A\" is: ",liste.index("A"))

print("\"B\" is {} times in list".format(liste.count("B")))

liste.sort() # Default sort alphabetical.

liste.sort(reverse=True) # Sort like Z~A or Bigger to smaller. 

print(liste)