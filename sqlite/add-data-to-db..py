import sqlite3

con = sqlite3.connect("db.db")
cursor = con.cursor()

def add_data():
  cursor.execute("insert into customers values('Muhammed','Uluel','Some Street,Some Avenue,Some City',5469326016)")
  con.commit()

def add_data_from_user(data):
  # cursor.execute("insert into customers values('{}','{}','{}',{})".format(data["name"],data["surname"],data["address"],data["phone"]))
  cursor.execute("insert into customers values(?,?,?,?)",data)
  con.commit()
  
name=input("Name: ")
surname=input("Surname: ")
address=input("Address: ")
phone=0000000000
while len(str(phone))!= 10:
  print("Please input 10 number(ex. 5xxxxxxxxx)")
  phone=int(input("Phone: "))
data = (name,surname,address,phone)
print(data, "Added to db")

add_data_from_user(data)

con.close()