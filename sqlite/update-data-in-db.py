from hashlib import new
import sqlite3

con = sqlite3.connect("db.db")
cursor = con.cursor()

def update_data(old_address,new_address):
  cursor.execute("Update customers set address = ? where address = ?",(new_address,old_address[0][0],))
  con.commit()

cursor.execute("select address from customers where name = ?",('Muhammed',))
old_address=cursor.fetchall()
print("Old Address is",old_address[0][0])
new_address="Yeni address"
update_data(old_address,new_address)
print("Address has been updated")
cursor.execute("select address from customers where name=?",("Muhammed",))
new_address=cursor.fetchall()
print("New Address is ",new_address[0][0])
con.close()