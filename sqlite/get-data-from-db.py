import sqlite3

con = sqlite3.connect("db.db")
cursor = con.cursor()

def get_data():
  cursor.execute("select * from customers")
  data = cursor.fetchall()
  return data

def search_data(data):
  cursor.execute("select * from customers where phone like '%{}%' ".format(data))
  data=cursor.fetchall()
  return data

data=get_data()
names = list()
surnames = list()
address = list()
phones = list()

print("### Customers ###")
for i in data:
  names.append(i[0])
  surnames.append(i[1])
  address.append(i[2])
  phones.append(i[3])
  print("""
  Name: {}
  Surname: {}
  Address: {}
  Phone: {}
  """.format(i[0],i[1],i[2],i[3]),end='')

print("********** Server-Side-Search **********")

number = 5469326016
if str(number).find("932"):
  print(number)
else:
  print("404")

print("********** Client-Side-Search **********")

inputt="932"
for number in phones:
  if inputt in str(number):
    searched_data = (search_data(number))
    for name,surname,address,phone in searched_data:
      name = name
      surname = surname
      address = address
      phone = phone

    print(name,surname,address,phone)

con.close()