import requests
from bs4 import BeautifulSoup

url = "http://www.imdb.com/chart/top"

response = requests.get(url)

html=response.content

soup = BeautifulSoup(html,"html.parser")

a=float(input("Imdb: "))

titles = soup.find_all("td",{"class":"titleColumn"})
ratings = soup.find_all("td",{"class":"ratingColumn imdbRating"})

for title,rating in zip(titles,ratings):
  title=title.text
  rating=rating.text
  title=title.replace("\n","")
  title=title.replace(" ","-")
  title=title.replace("------"," ")
  title=title.replace("-"," ")
  title=title.replace("("," (")
  rating=rating.replace("\n","")
  if float(rating)>a:
    print("Title: {} - Rating: {}".format(title,rating))
