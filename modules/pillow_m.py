from PIL import Image, ImageFilter

image = Image.open("modules/pillow_images/golf.jpg")

image.save("modules/pillow_images/mini_golf.jpg")

image.rotate(180).save("modules/pillow_images/golf3.jpg")

image.rotate(90).save("modules/pillow_images/golf4.jpg")

image.convert(mode="L").save("modules/pillow_images/golf5.jpg")

degistir = (960,600)

image.thumbnail(degistir)

image.save("modules/pillow_images/golf_thumb.jpg")

image.filter(ImageFilter.GaussianBlur(100)).save("modules/pillow_images/golf_blur.jpg")

cut_area=(1000,400,3450,1600)
image.crop(cut_area).save("modules/pillow_images/golf_cut.jpg")