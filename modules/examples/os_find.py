import os,fnmatch

def find(name,path):
  for root,dirs,files in os.walk(path):
    if name in files:
      return os.path.join(root,name)

def find_all(name,path):
  result=[]
  for root,dirs,files in os.walk(path):
    if name in files:
      result.append(os.path.join(root,name))
  return result

def find_match(patterns, path):
    for root, dirs, files in os.walk(path):
        for name in files:
          for pattern in patterns:
            if fnmatch.fnmatch(name, pattern) and pattern == "*.pdf":
              with open("modules/examples/files/pdf_dosyalari.txt","a",encoding="utf-8") as f:
                f.write(os.path.join(root, name)+"\n")
            elif fnmatch.fnmatch(name, pattern) and pattern == "*.mp4":
              with open("modules/examples/files/mp4_dosyalari.txt","a",encoding="utf-8") as f:
                f.write(os.path.join(root, name)+"\n")
            elif fnmatch.fnmatch(name, pattern) and pattern == "*.txt":
              with open("modules/examples/files/txt_dosyalari.txt","a",encoding="utf-8") as f:
                f.write(os.path.join(root, name)+"\n")
    return "Dosyalar Hazir"

print(find_match(('*.pdf','*.mp4','*.txt'), 'c:/users/n1nsh4/desktop'))