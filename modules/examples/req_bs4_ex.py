from locale import currency
import requests 
from bs4 import BeautifulSoup

url="https://www.doviz.com/"

response=requests.get(url)

html=response.content

soup = BeautifulSoup(html,"html.parser")

dolar=soup.find("span",{"class":"value","data-socket-key":"USD"}).string.replace(',','.')
euro=soup.find("span",{"class":"value","data-socket-key":"EUR"}).string.replace(',','.')
altin=soup.find("span",{"class":"value","data-socket-key":"gram-altin"}).string.replace(',','.')
borsa=soup.find("span",{"class":"value","data-socket-key":"XU100"}).string.replace(".",'').replace(',','.')

print(dolar,euro,altin,borsa)

usd = (float(borsa) / float(dolar))


print("""
Borsada hisse alabilmek için: {} TL ye yani {} USD ye ihtiyaciniz var. 
""".format(borsa,usd)
)
