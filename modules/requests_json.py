import requests as r
import sys
url="http://data.fixer.io/api/latest?access_key=3b26183123c8809e9cf900baf981342b"
while True:
  currency1=input("First currency: ").upper()
  currency2=input("Second currency: ").upper()
  amount = int(input("Amount : "))
  response = r.get(url)
  json=response.json()
  try:
    first_currency=json["rates"][currency1]
    second_currency=json["rates"][currency2]

    second_currency = second_currency / first_currency
    first_currency = first_currency / first_currency

    print("""
    {} {} = {} {}. 
    {} {} = {} {}.
    """.format(first_currency,currency1,second_currency,currency2,(first_currency*amount),currency1,(second_currency*amount),currency2))
  except KeyError:
    sys.stderr.write("Please check the entered currency!\n")
    sys.stderr.flush()
  close=input("Do you want to close program? (y/N) :")
  if close.lower()=="y" or close.lower()=="q":
    break