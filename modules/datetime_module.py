from datetime import datetime
import locale

locale.setlocale(locale.LC_ALL,"Turkish_Turkey")

now = datetime.now()

print(now)
print(now.year)
print(now.month)
print(now.day)
print(now.hour)
print(now.minute)
print(now.second)
print(now.date())
print(now.ctime())
print(now.combine(now.date(),now.time()))
print(now.utctimetuple())
print(now.utcnow())
print(now.utcfromtimestamp(0))
print(datetime.fromtimestamp(0))
print(now.weekday())
print(now.isoweekday())
print(datetime.fromisocalendar(1998,34,7))
print(now.isoweekday())
print(dir(datetime))
print(dir(now))

print(datetime.strftime(now,('%Y %B %A')))
