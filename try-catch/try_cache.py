# try:
#   print("Hello text.")
#   a= int("32jd")
# except ValueError:
#   print("Value error occured.")
# except:
#   print("An error occurred.")


try:
  x = int(input("Enter the divided number : "))
  y = int(input("Enter the divisor number : "))
  print(x/y)
except ValueError:
  print("Wrong input entered, Please check!")
except ZeroDivisionError:
  print("No number can be divided by zero.")
except:
  print("Unknown error occurred. We are sorry!")