from sqlalchemy import null


def double(x):
  return x*2
  
print(list(map(double,[1,2,3,4,5,6,7])))

print(list(map(lambda x : x ** 2, (1,2,3,4,5,6,7,8,9,10))))

liste1=[32,325,421,412,False]
liste2=[6,4234,574,37,52]
print(list(map(lambda x,y : x*y,liste1,liste2)))