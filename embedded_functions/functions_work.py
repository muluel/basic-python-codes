# Work 1
rectangle = [(3,4),(10,3),(5,6),(1,9)]
print(list(map(lambda x : x[0] * x[1],rectangle )))

# Work 2
def is_triangle(triangle):
  if abs(triangle[0]+triangle[1])>triangle[2] and abs(triangle[0]+triangle[2])>triangle[1] and abs(triangle[2]+triangle[1])>triangle[0]:
    return True
  else:
    return False

triangle = [(3,4,5),(6,8,10),(3,10,7),(3,9,7)]
print(list(filter(is_triangle,triangle))) 

# Work 3
from functools import reduce
liste = [1,2,3,4,5,6,7,8,9,10]
filtered_list = filter(lambda x :x%2==0, liste)
print(reduce(lambda x,y:x+y ,filtered_list))

# Work 4
names = ["Kerim","Tarık","Ezgi","Kemal","İlkay","Şükran","Merve"]
surnames = ["Yılmaz","Öztürk","Dağdeviren","Atatürk","Dikmen","Kaya","Polat"]
[ print(i + " " + j) for i,j in list(zip(names,surnames)) ]