# Example 1
class Text():
  def find_letter(self,text):
    times=dict()
    for i in text:
      if i in times:
        times[i]+=1
      else:
        times[i]=1
    for i,j in times.items():
      print(i,":",j,"times pass.")

text="ProgramlamaÖdeviİleriSeviyeVeriYapılarıveObjeleripynb"
example=Text()
example.find_letter(text)

# Example 2
class Poem():
  def __init__(self) -> None:
    self.first_letters=""
    with open("./data-sturctures-and-classes/poem.txt","r",encoding="utf-8") as file:
        for i in file:
          self.first_letters += i[0]
    print(self.first_letters)

fist_poem=Poem() 

# Example 3
with open("./data-sturctures-and-classes/mailler.txt","r",encoding="utf-8")as file:
  for line in file:
    line = line.strip("\n")
    if (line.startswith(".",-4) or line.startswith(".",-3)) and line.find("@") !=-1:
      print(line)

# Example 4
names =["Kerim","Tarık","Ezgi","Kemal","İlkay","Şükran","Merve"]
last_names = ["Yılmaz","Öztürk","Dağdeviren","Atatürk","Dikmen","Kaya","Polat"]
full_names= list(zip(names,last_names))
full_names.sort()
for i,j in full_names:
  print(i,j)
