class Kareler():
  def __init__(self,maksimum=0) -> None:
      self.maksimum=maksimum
      self.index=0

  def __iter__(self):
    return self
  
  def __next__(self):
    if self.index<=self.maksimum:
      print(self.index**2)
      self.index+=1
    else:
      raise StopIteration

kareler=Kareler(10)
iteration=iter(kareler)

next(iteration)

# Generator

def prime(num):
  i = 2
  while i<num:
    if (num%i==0):
      return False
    i+=1
  return True

def prime_generator():
  i=2
  while True:
    if(prime(i)):
      yield i
    i+=1

for num in prime_generator():
  if (num>1000):
    break
  print(num)
