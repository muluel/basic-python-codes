from math import prod
import sqlite3

from numpy import product

class Product():
  def __init__(self,name,brand,price,category,expiry_date,stock) -> None:
      self.name = name
      self.brand = brand
      self.price = price
      self.category = category
      self.expiry_date = expiry_date
      self.stock = stock

  def __str__(self):
    return"Product Name : {}\n-Brand : {}\n-Price : {}\n-Category : {}\n-Expiry Date : {}\n-Stock : {}\n".format(self.name,self.brand,self.price,self.category,self.expiry_date,self.stock)


class Mall():
  def __init__(self) -> None:
      self.connection()
  
  def connection(self):
    self.con=sqlite3.connect("./mall/mall.db")
    self.cursor=self.con.cursor()
    query = "Create Table if not exists products (name TEXT,brand TEXT,price INT,category TEXT,expiry_date TEXT,stock INT)"
    self.cursor.execute(query)
    self.con.commit()

  def close_connection(self) -> None:
    self.con.close()

  def show_products(self):
    q="select * from products"
    self.cursor.execute(q)
    products= self.cursor.fetchall()
    if len(products)==0:
      return print("No products in the mall.")
    else:
      for i in products:
        product=Product(i[0],i[1],i[2],i[3],i[4],i[5])
        print(product)

  def buy_product(self,name,qty):
    q="Select * from products where name=?"
    self.cursor.execute(q,(name,))
    product = self.cursor.fetchall()
    if len(product)==0:
      return print("Product can not found.")
    elif product[0][5]==0:
      return print("This product is out of stock.")
    else:
      new_qty=product[0][5]-qty
      q="Update products set stock=? where name=?"
      self.cursor.execute(q,(new_qty,name))
      self.con.commit()
      return print("Thanks for choosing us, Have a good day.")

  def add_product(self,name,brand,price,category,expiry_date,stock):
    try:
      name = name
      brand = brand
      price = price
      category = category
      expiry_date = expiry_date
      stock = stock
      q="insert into products values(?,?,?,?,?,?)"
      self.cursor.execute(q,(name,brand,price,category,expiry_date,stock))
      self.con.commit()
      return print("Product added to mall.")
    except:
      return print("An error occurred please try again.")
