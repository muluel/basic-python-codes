from mall_framework import Mall

mall=Mall()
print("""
*****************************
Welcome To Mall

1.Show Product

2.Buy Product

2.Add Product

For quit enter 'q'.
*****************************
""")

while True:
  action= input("What would you like to do? : ")
  if action=="q":
    break
  elif action=="1":
    mall.show_products()
  elif action=="2":
    name= input("Name of Product : ")
    qty = input("How many want to buy : ")
    try:
      int(qty)
      mall.buy_product(name,qty)
    except:
      print("Please enter a number for quantity.")
  elif action=="3":
    try:
      name = input("Name of Product : ")
      brand = input("Brand of Product : ")
      price = input("Price of Product : ")
      category = input("Category of Product : ")
      expiry_date = input("Expiry Date of Product : ")
      stock = input("How many : ")
      mall.add_product(name,brand,price,category,expiry_date,stock)
    except:
      print("Invalid input please check the input.")
  else:
    print("Invalid input please check!")