import math
import sqlite3
import math
class Song():
  def __init__(self,name,artist,album,genre,production_company,duration) -> None:
      self.name = name
      self.artist = artist
      self.album = album
      self.genre = genre
      self.production_company = production_company
      self.duration = duration

  def __str__(self)->str:
    seconds = self.duration%60
    minutes = math.floor(self.duration/60)
    hours = math.floor(minutes/60)
    duration=str(str("{:02d}".format(minutes)) + ":" if hours==0 else str("{:02d}".format(hours))+":"+str("{:02d}".format(minutes%60))+":") + str("{:02d}".format(seconds))
    return "Song Name : {}\n-Artist : {}\n-Album : {}\n-Genre : {}\n-Production Company : {}\n-Duration : {}\n".format(self.name,self.artist,self.album,self.genre,self.production_company,duration)

class Playlist():
  def __init__(self) -> None:
      self.connection()

  def connection(self):
    self.con=sqlite3.connect("./song-app/songs.db")
    self.cursor=self.con.cursor()
    q= "Create table if not exists songs (id int PRIMARY KEY AUTOINCREMENT, name TEXT,artist TEXT,album TEXT,genre TEXT,production_company TEXT,duration int)"
    self.cursor.execute(q)
    self.con.commit()
  
  def close_connection(self):
    self.con.close()

  def total_playlist_time(self):
    q="select sum(duration) from Songs"
    self.cursor.execute(q)
    total_duration = self.cursor.fetchall()
    minutes = math.floor(total_duration[0][0]/60)
    hours = math.floor(minutes/60)
    days = math.floor(hours/24)
    minutes = "{:02d}".format(minutes%60)
    seconds = "{:02d}".format(total_duration[0][0]%60)
    total_duration = str("" + str("" if hours==0 else str(hours)+":") if days == 0 else str(days) +" days "+ str("{:02d}".format(days%24)+":")) + str("" if minutes==0 else str(minutes)+":")+str(seconds)
    return print("Total duration of playlist is : {}".format(total_duration))

  def show_songs(self):
    q = "select * from songs"
    self.cursor.execute(q)
    songs = self.cursor.fetchall()
    if len(songs)==0:
      return print("There is no song in playlist")
    else:
      for i in songs:
        song = Song(i[1],i[2],i[3],i[4],i[5],i[6])
        print(song)
      
  def add_song(self,song):
    q = "Insert into songs (name,artist,album,genre,production_company,duration) values(?,?,?,?,?,?)"
    self.cursor.execute(q,song)
    self.con.commit()
    return print("Song adding to playlist...")

  def delete_song(self,name):
    q="Select * from Songs where name = ?"
    self.cursor.execute(q,(name,))
    song=self.cursor.fetchall()
    if len(song)==0:
      return print("There is no song like {}.\nPlease check the name.".format(name))
    else:
      dq="delete from songs where name = ?"
      self.cursor.execute(dq,(name,))
      self.con.commit()
      return print("Song deleted from playlist.")

  def search_song(self, name):
    q="select * from songs where name like '%{}%' ".format(name)
    self.cursor.execute(q)
    songs = self.cursor.fetchall()
    if len(songs)==0:
      return print("There is no song like {} in this playlist.".format(name))
    else:
      for i in songs:
        song=Song(i[1],i[2],i[3],i[4],i[5],i[6])
        return print(song)
