from song_framework import Playlist
import time

print("""
**************************
Welcome To Library

1. Show Song

2. Search Song

3. Add Song

4. Delete Song

5. Total Duration Time

For quit press "q".
***************************
""")
playlist=Playlist()
while True:
  action=input("What would you like to do : ")
  if (action=="q"):
    print("The app is closing...")
    break
  elif(action=="1"):
    playlist.show_songs()
  elif(action=="2"):
    name = input("Which book do you want to search : ")
    print("Songs are searching...")
    time.sleep(2)
    playlist.search_song(name)
  elif(action=="3"):
    try:
      name=input("Name : ")
      artist=input("Artist : ")
      album=input("Album : ")
      genre=input("Genre : ")
      production_company=input("Production Company : ")
      duration=int(input("Duratin(second) : "))
      if type(duration)!=int:
        print("Please enter number:")
        duration=int(input("Duratin(second) : "))
      new_song = (name,artist,album,genre,production_company,duration)
      print("Adding song..")
      time.sleep(2)
      playlist.add_song(new_song)
      print("Song added.")
    except:
      print("An error accoured, Please Check the given information")
      pass
  elif(action=="4"):
    name=input("Name : ")
    sure=input("Are you sure?(y/N)")
    if sure.lower()=="y":
      playlist.delete_song(name)
    else:
      print("Action is canceling.")
      time.sleep(2)
      print("Action cancelled.")
  elif(action=="5"):
    playlist.total_playlist_time()
  else:
    print("Error 404! Action not found.")
