class Person():
  name = "Muhammed"
  surname = "Uluel"
  gender = "Male"
  date_of_birth = "23.08.1998"

muhammed = Person()

print("""  Person Information

  Name : {}
  Surname : {}
  Gender : {}
  Date of birth : {}""".format(muhammed.name,muhammed.surname,muhammed.gender,muhammed.date_of_birth))

# Class with init function.
class Personinit():
  def __init__(self,name,surname,gender,date_of_birth):
    self.name = name
    self.surname = surname
    self.gender  = gender
    self.date_of_birth = date_of_birth

  def show_info(self):
    print("""
    Person Information
    
    Name : {}
    Surname : {}
    Gender : {}
    Date of birth : {}
    """.format(self.name,self.surname,self.gender,self.date_of_birth))

muhammedinit = Personinit("Muhammed_init","Uluel","Male","15.08.1999")
muhammedinit.show_info()

class Job():
  def __init__(self,title,qty,price,company,status):
    self.title = title
    self.qty = qty
    self.price = price
    self.company = company
    self.status = status
    print("Job has been created.")
  
  def stats(self):
    print("""
    Job : {}
    Quantity : {}
    Price : {}
    Status : {}
    """.format(self.title,self.qty,self.price,self.status))
  
  def change_qty(self,new_qty):
    self.qty  = new_qty
    print("Quantity of job has been changed.")

  def done(self):
    self.status = True
    print("Job marked as \"Done\"")

  def total_price(self):
    print(self.price*self.qty)
    return self.price*self.qty

tshirt = Job("22203001 Polo",1250,12.50,"2512 Tekstil",False)

tshirt.stats()
tshirt.change_qty(1500)
tshirt.stats()
tshirt.done()
tshirt.stats()

class OutJob(Job):
  def __init__(self, title, qty, price, company, status, sub_title, co_company,cost,paid):
      super().__init__(title, qty, price, company, status)
      self.sub_title = sub_title
      self.co_company = co_company
      self.cost = cost
      self.paid = paid

  def total_cost(self):
    print(self.cost*self.qty)
    return self.cost*self.qty
  
  def is_paid(self):
    self.paid = True
    print("{} is marked as paid in the future this not calculate for dept.".format(self.sub_title.capitalize()))

kemer = OutJob("Esofman",1200,13,"2512 Tekstil",False,"kemer","Halit Tekstil",.88,False)

kemer.stats()
print(kemer.status)
kemer.total_cost()
kemer.total_price()
kemer.change_qty(1234)

total_earn=kemer.total_price() - kemer.total_cost()
print("Total earn from {} is {:,.2f} TL.".format(kemer.title,total_earn))

kemer.is_paid()
print(dir(kemer))


#zip function usage
a=("ad","soyad")
b=("muhammed","uluel")
c = zip(a,b)

for i,j in c:
  print(i+":  "+j)