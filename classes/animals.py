class Animal():
  def __init__(self,name='undefined',age=0,) -> None:
      self.name = name
      self.age = age
  def __str__(self) -> str:
      return "Name: "+ self.name +"\nAge: "+ str(self.age)

class Dog(Animal):
  def __init__(self, name='undefined', age=0,leg=0) -> None:
      super().__init__(name, age)
      self.leg=leg

  def __str__(self) -> str:
      return super().__str__() + "\nLeg: " + str(self.leg)

class Horse(Animal):
  def __init__(self, name='undefined', age=0,leg=0) -> None:
      super().__init__(name, age)
      self.leg=leg

  def __str__(self) -> str:
      return super().__str__() + "\nLeg: " + str(self.leg)

class Bird(Animal):
  def __init__(self, name='undefined', age=0,fly=True) -> None:
      super().__init__(name, age)
      self.fly=fly

  def __str__(self) -> str:
      return super().__str__() + "\nFly: " + str(self.fly)

dog=Dog("Pulsar",7,4)
print(dog)
horse=Horse("Duldul",31,4)
print(horse)
bird=Bird("Twety",7)
print(bird)