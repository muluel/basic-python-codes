from re import A


class Computer():
  def __init__(self,brand,model,kind,purpose,price,production_date,cpu,ram,hdd,ssd,gpu,gpu_ram_size,os,screen_resolution,screen_size,touch_screen,weight,thickness,battery_capacity,battery_life,usb_ports,display_ports,color,stock):
    self.brand = brand
    self.model = model
    self.kind = kind
    self.purpose = purpose
    self.price = price
    self.production_date = production_date
    self.cpu = cpu
    self.ram = ram
    self.hdd = hdd
    self.ssd = ssd
    self.gpu = gpu
    self.gpu_ram_size = gpu_ram_size
    self.os = os
    self.screen_resolution = screen_resolution
    self.screen_size = screen_size
    self.touch_screen = touch_screen
    self.weight = weight
    self.thickness = thickness
    self.battery_capacity = battery_capacity
    self.battery_life = battery_life
    self.usb_ports = usb_ports
    self.display_ports = display_ports
    self.color = color
    self.stock = stock

  def __str__(self):
    return self.brand +' '+ self.model +' '+ self.cpu +' '+ self.gpu_ram_size +' '+ self.gpu +' '+ self.hdd +' '+ self.ssd +' '+ self.ram +' '+ str(self.stock)

  def __len__(self):
    return self.stock

  def change_property_value(self,property,new_value):
    properties=['brand','model','kind','purpose','price','production_date','cpu','ram','hdd','ssd','gpu','gpu_ram_size','os','screen_resolution','screen_size','touch_screen','weight','thickness','battery_capacity','battery_life','usb_ports','display_ports','color','stock']
    if property in properties:
      if property == "brand":
        self.brand = new_value
      elif property == "model":
        self.model = new_value
      elif property == "kind":
        self.kind = new_value
      elif property == "purpose":
        self.purpose = new_value
      elif property == "price":
        self.price = new_value
      elif property == "production_date":
        self.production_date = new_value
      elif property == "cpu":
        self.cpu = new_value
      elif property == "ram":
        self.ram = new_value
      elif property == "hdd":
        self.hdd = new_value
      elif property == "ssd":
        self.ssd = new_value
      elif property == "gpu":
        self.gpu = new_value
      elif property == "gpu_ram_size":
        self.gpu_ram_size = new_value
      elif property == "os":
        self.os = new_value
      elif property == "screen_resolution":
        self.screen_resolution = new_value
      elif property == "screen_size":
        self.screen_size = new_value
      elif property == "touch_screen":
        self.touch_screen = new_value
      elif property == "weight":
        self.weight = new_value
      elif property == "thickness":
        self.thickness = new_value
      elif property == "battery_capacity":
        self.battery_capacity = new_value
      elif property == "battery_life":
        self.battery_life = new_value
      elif property == "usb_ports":
        self.usb_ports = new_value
      elif property == "display_ports":
        self.display_ports = new_value
      elif property == "color":
        self.color = new_value
      elif property == "stock":
        self.stock = new_value
    else:
      print("Property Not Found. Please check!")
    print("\"{}\" value of Class is changed.".format(property))

brand = "Asus"
model = "XXXC2"
kind  = "Notebook"
purpose = "Complex"
price = 799
production_date = 2015
cpu = "Intel i5"
ram = "8GB"
hdd = "1TB"
ssd = "120GB"
gpu = "Nvidia GTX 1080"
gpu_ram_size = "2GB"
os = "FreeDos"
screen_resolution = 1080
screen_size = "14\"" 
touch_screen = "No"
weight = 1400
thickness = 155
battery_capacity = 3000
battery_life = 5
usb_ports = ['2xUSB V3',"1xUSB V2"]
display_ports = ["1xHDMI","1xVGA"]
color = "Silver"
stock = 15

asus = Computer(brand,model,kind,purpose,price,production_date,cpu,ram,hdd,ssd,gpu,gpu_ram_size,os,screen_resolution,screen_size,touch_screen,weight,thickness,battery_capacity,battery_life,usb_ports,display_ports,color,stock)

print(asus)
asus.change_property_value("cpu","Intel i7")
print(asus)
print(len(asus))

features={
"brand" : "Asus",
"model" : "XXXC2",
"kind" : "Notebook",
"purpose" : "Complex",
"price" : 799,
"production_date" : 2015,
"cpu" : "Intel i5",
"ram" : "8GB",
"hdd" : "1TB",
"ssd" : "120GB",
"gpu" : "Nvidia GTX 1080",
"gpu_ram_size" : "2GB",
"os" : "FreeDos",
"screen_resolution" : 1080,
"screen_size" : "14\"" ,
"touch_screen" : "No",
"weight" : 1400,
"thickness" : 155,
"battery_capacity" : 3000,
"battery_life" : 5,
"usb_ports" : ['2xUSB V3',"1xUSB V2"],
"display_ports" : ["1xHDMI","1xVGA"],
"color" : "Silver",
"stock" : 15,
}

asus2 = Computer(features["brand"],features["model"],features["kind"],features["purpose"],features["price"],features["production_date"],features["cpu"],features["ram"],features["hdd"],features["ssd"],features["gpu"],features["gpu_ram_size"],features["os"],features["screen_resolution"],features["screen_size"],features["touch_screen"],features["weight"],features["thickness"],features["battery_capacity"],features["battery_life"],features["usb_ports"],features["display_ports"],features["color"],features["stock"])

print(asus2)
asus2.change_property_value("cpu","Intel i7")
print(asus2)
print(len(asus2))