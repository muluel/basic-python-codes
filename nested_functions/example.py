import numbers
from decorator_functions import benchmark

def perfect_number(numbers):
  def decorator(func):
    from functools import wraps
    @wraps(func)
    def wrapper(numbers):
      perfect_list=[]
      for perfect in numbers:
        sum=0
        for i in range(1,perfect):
          if (perfect%i) == 0:
            sum=sum+i
        if sum == perfect:
          perfect_list.append(perfect)
      print("Perfect Numbers : ",perfect_list)
      result=func(numbers)
      return result
    return wrapper
  return decorator

@benchmark
@perfect_number(numbers)
def prime_number(numbers):
  prime_list=[]
  for prime in numbers:
    if prime==2 or prime==3:
      prime_list.append(prime)
    else:
      for i in range(2,prime+1):
        if (prime%i)==0:
          break
        elif (prime%i)==1:
          continue
        else:
          prime_list.append(prime)
          break
  return print("Prime Numbers : ",prime_list)

prime_number(range(1,1001))