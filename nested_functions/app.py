from flask import Flask,render_template
from decorators_with_parameter import has_permission
from decorator_functions import benchmark
from decorators_in_class import Sleep

app=Flask(__name__)

@app.route('/',methods=['GET'])
def home():
  return render_template('')
  
@benchmark
@Sleep(3)
class User(object):
  is_active = True
  def __init__(self, name, permissions):
      """
      :param name: string
      :param permissions: list
      """
      self.name = name
      self.permissions = permissions

# Chained usage
@benchmark
@has_permission('can_view_dashboard')
def view_dashboard(**kwargs):
    user = kwargs.pop('user')

    print('\n- DASHBOARD')
    print('- Merhaba {username}, giriş başarılı.'.format(
      username=user.name
      ))



@benchmark
def test_status_200():
  from urllib import request
  response=request.urlopen(request.Request("https://httpbin.org/status/200",method='HEAD'))

  assert response.status==200

@benchmark
@Sleep(2)
def hello(name):
  print("Hello {}".format(name))


user = User('muhammed', ['can_view_dashboard','is_superuser','editor','can_delete_post'])
view_dashboard(user=user)

hello("World")
test_status_200()