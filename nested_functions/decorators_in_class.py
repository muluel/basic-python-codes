class Sleep:
  def __init__(self,secs) -> None:
      self.secs = secs
  
  def __call__(self,func):
    from functools import wraps

    @wraps(func)
    def wrapper(*args, **kwargs):
      from time import sleep

      sleep(self.secs)
      return func(*args, **kwargs)

    return wrapper
