# Decorators with Patameter


# This decorator has permission parameter
def has_permission(permission):
  def decorator(func):
    from functools import wraps

    @wraps(func)
    def wrapper(*args, **kwargs):
      user = kwargs.get('user')
      assert permission in user.permissions

      result = func(*args, **kwargs)
      return result

    return wrapper

  return decorator
