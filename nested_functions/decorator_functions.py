import time
def benchmark(func):
  def wrapper(*args, **kwargs):
    time_start = time.time()
    result=func(*args,**kwargs)
    time_end = time.time()
    time_delta = time_end-time_start

    print("The [{}] function it took {} seconds.".format(func.__name__,time_delta))
    return result
  return wrapper
