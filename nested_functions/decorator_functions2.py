import time
def extra(func):
  def wrapper(numbers):
    dual_sum=0
    dual_numbers=0
    odd_sum=0
    odd_numbers=0

    for num in numbers:
      if (num%2)==0:
        dual_sum+=num
        dual_numbers+=1
      else:
        odd_sum+=num
        odd_numbers+=1
    
    print("Odd avarage : {}".format(odd_sum/odd_numbers))
    print("Dual avarage : {}".format(dual_sum/dual_numbers))

    func(numbers)
  return wrapper

@extra
def find_avarage(numbers):
  sumofnum=0
  for num in numbers:
    sumofnum+=num
  print("Avarage : ",sumofnum/len(numbers))

find_avarage((1,4,4,5,6,6,32,5,1,3))
